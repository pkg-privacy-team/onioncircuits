# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-14 15:45+0100\n"
"PO-Revision-Date: 2019-01-15 12:27+0000\n"
"Last-Translator: erinm\n"
"Language-Team: Slovenian (http://www.transifex.com/otf/torproject/language/"
"sl/)\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || "
"n%100==4 ? 2 : 3);\n"

#: ../onioncircuits:78
msgid "You are not connected to Tor yet..."
msgstr ""

#: ../onioncircuits:89
msgid "Onion Circuits"
msgstr ""

#: ../onioncircuits:119
msgid "Close this circuit"
msgstr ""

#: ../onioncircuits:132
msgid "Circuit"
msgstr ""

#: ../onioncircuits:133
msgid "Status"
msgstr "Stanje"

#: ../onioncircuits:151
msgid "Click on a circuit for more detail about its Tor relays."
msgstr ""

#: ../onioncircuits:261
msgid "The connection to Tor was lost..."
msgstr ""

#: ../onioncircuits:355
msgid ", "
msgstr ""

#: ../onioncircuits:357
msgid "..."
msgstr ""

#: ../onioncircuits:387
#, c-format
msgid "%s: %s"
msgstr "%s: %s"

#: ../onioncircuits:617
msgid "GeoIP database unavailable. No country information will be displayed."
msgstr ""

#: ../onioncircuits:647
#, c-format
msgid "%s (%s)"
msgstr ""

#: ../onioncircuits:651
#, c-format
msgid "%.2f Mb/s"
msgstr ""

#: ../onioncircuits:653 ../onioncircuits:654
msgid "Unknown"
msgstr "Neznan"

#: ../onioncircuits:669
msgid "Fingerprint:"
msgstr "Prstni odtis:"

#: ../onioncircuits:670
msgid "IP:"
msgstr ""

#: ../onioncircuits:671
msgid "Bandwidth:"
msgstr ""
