#!/bin/sh
# autopkgtest check: Run GUI test suite for Onion Circuits.
# Author: Sascha Steinbiss <satta@debian.org>
set -e

printf "waiting for Tor to settle...\n"
sleep 20

WORKDIR=$(mktemp -d)

# we don't always have a home directory, e.g. on buildds
export XDG_CONFIG_HOME=$WORKDIR/.config
export XDG_DATA_HOME=$WORKDIR/.local/share
export XDG_CACHE_HOME=$WORKDIR/.cache
export XDG_RUNTIME_DIR=$WORKDIR/runtime
mkdir -p $XDG_RUNTIME_DIR

# start Xvfb
# note that the screen size must be that large to avoid UI elements
# going off screen, which would cause dogtail to fail with an error
(Xvfb :5 -screen 0 1600x1200x24 -ac -noreset -v -fbdir $WORKDIR/ >/dev/null 2>&1 &)
XVFB_PID=$!

# XXX attach VNC session for local debugging only
#x11vnc -ncache 10 -display :5 &
#sleep 2
#vncviewer localhost &

# finish setting up X
export DISPLAY=:5
export XAUTHORITY=/dev/null

# start local D-Bus session
dbus-daemon --fork --session --print-address=3 --print-pid=4 \
    3> /tmp/dbus-session-bus-address 4> /tmp/dbus-session-bus-pid
export DBUS_SESSION_BUS_ADDRESS="$(cat /tmp/dbus-session-bus-address)"
DBUS_SESSION_BUS_PID="$(cat /tmp/dbus-session-bus-pid)"

# register clean up handler
trap "rm -rf $WORKDIR && kill $DBUS_SESSION_BUS_PID $XVFB_PID" 0 INT QUIT ABRT PIPE TERM

# enable assistive access, required for dogtail
gsettings set org.gnome.desktop.interface toolkit-accessibility true

# make sure no other instance is running, as it confuses dogtail
killall -q onioncircuits || true

# run test
debian/tests/check-circuits
